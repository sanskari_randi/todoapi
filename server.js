var express = require('express');
var bodyParser = require('body-parser');

var app = express();

var PORT = process.env.PORT || 3000 ;

var todos = []; // holds all the items
var todoNextId = 1 ; //we are going to use this to increment id

app.use(bodyParser.json()); // middleware to parse every request to json

app.get('/' , function(req, res){
    res.send('Todo API root'); // home or root of the site
});

 //get all todos
app.get('/todos', function(req, res){
   res.json(todos); // sending the whole todos array as response
});

//get indvidual todos

app.get('/todos/:id', function(req, res){
    var todoId = parseInt(req.params.id,10);// extract the id from url . and 10 is the base here
    var matchedTodo;
    todos.forEach(function(arrayitem){ // iterate thru each array item
        if (todoId === arrayitem.id){ //if url id matches with any of the array id
            matchedTodo = arrayitem; // then assign it to the variable
        };
    });
    if(matchedTodo){  // if matchedtodo is not null or false then send it as response
        res.json(matchedTodo);
    }else{
        res.status(404).send(); // else send error status 404 not found
    }
});

 // POST methods
 // we will send json object along with our request and it will
 // be saved in the todos arrays

app.post('/todos', function(req, res){
    var body = req.body;

    body.id = todoNextId ;
    todoNextId++;

    // wrong way not incrementing id
    //todos.push({
    //    //id: id + todoNextId ,
    //    description : body.description,
    //    completed : body.completed
    //});

    //right way

    todos.push(body);

    //console.log('description ' + body.description);

    res.json(body);
});



app.listen(PORT,function(){
    console.log('server listening on PORT : '  + PORT);
});